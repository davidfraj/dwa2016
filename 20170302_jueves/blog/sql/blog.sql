-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-03-2017 a las 20:56:07
-- Versión del servidor: 5.6.14
-- Versión de PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `blog`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `contenido` text COLLATE utf8_spanish_ci NOT NULL,
  `autor` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id`, `titulo`, `contenido`, `autor`, `imagen`, `fecha`) VALUES
(1, 'El Parque Deportivo Ebro funcionará a pleno rendimiento la próxima temporada', 'El Parque Deportivo Ebro funcionará a pleno rendimiento la próxima temporada. La Fundación Educación, Salud y Deporte de Aragón (ESDA) tiene previsto acometer próximamente la segunda fase del proyecto de las nuevas instalaciones, que deberían estar listas en septiembre. El presupuesto estimado para las obras es de 6.123.884 euros e irá destinado fundamentalmente al acondicionamiento de la piscina cubierta (1.995.000) y a la construcción de residencias para deportistas y estudiantes (2.026.000).', 'david', '', '2017-03-02'),
(2, 'Una piscina eficiente gracias a la luz solar... y a las heces de caballo', 'La piscina del Parque Deportivo Ebro -la única cubierta pública de 50 metros de la Comunidad- volverá a ser el buque insignia de las instalaciones. El Gobierno de Aragón determinó su cierre en 2012 por su elevado coste de mantenimiento (más de 300.000 euros anuales), pero la Fundación ESDA se ha comprometido a hacerla eficiente a través de un cambio en el sistema energético. Hasta su abandono, la temperatura del agua (28ºC) y del espacio cerrado se mantenían con una caldera de gasoil; a partir de la próxima temporada, la luz solar y la biomasa la harán autosostenible.', 'heraldo', '', '2017-03-02'),
(3, 'Una niña zaragozana le pide a Sergio Llull que le enseñe a tirar triples desde el centro del campo', 'Sergio Llull es uno de los jugadores del momento. El gran dominador del baloncesto europeo esta temporada con canastas y partidos solo al alcance de las mejores estrellas. Y una estrella, un ídolo absoluto, es Sergio Llull para Julia de Castro, una niña zaragozana que se fotografió con él hace unos meses en la puerta del pabellón Príncipe Felipe y que ahora le ha escrito una emotiva carta que le ha tratado de hacer llegar a través de las redes sociales. ', 'heraldo', '', '2017-03-01'),
(4, 'Motorland ya mira al Mundial de Superbikes', 'Un Mundial que llega con renovados bríos. A la parrilla se han incorporado nombres propios, como el del alemán Stefan Bradl, que ha hecho su debut en la categoría reina de Superbikes después de muchas temporadas en Moto GP. En las filas de Honda se ha encontrado a todo un veterano, el estadounidense Nicky Hayden, de 34 años, que se proclamó campeón del mundo de Moto GP en 2006, que dio el salto a esta competición el año pasado, curso en el que solo ganó una carrera.', 'heraldo', '', '2017-03-02');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
