<?php
require('datos.php');

//Intento recoger la variable por GET llamada c
if(isset($_GET['c'])){
	$c=$_GET['c'];
}else{
	$c=0;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>got.php</title>
	<script type="text/javascript" href="funciones.js"></script>
	<link rel="stylesheet" href="estilos.css">
</head>
<body>

	<section>
		<nav>
			<!-- a{$}[href=""]*10 -->
			<a href="got.php?c=0">1</a>
			<a href="got.php?c=1">2</a> 
			<a href="got.php?c=2">3</a> 
			<a href="got.php?c=3">4</a> 
			<a href="got.php?c=4">5</a> 
			<a href="got.php?c=5">6</a> 
			<a href="got.php?c=6">7</a> 
			<a href="got.php?c=7">8</a> 
			<a href="got.php?c=8">9</a> 
			<a href="got.php?c=9">10</a>

		</nav>
		<header>
			<hr>
			
			<?php if($c==0){ echo 'Anterior';}else{ ?>
			<a href="got.php?c=<?php echo $c-1;?>">Anterior</a>
			<?php } ?>
			
			<?php 
			if($c==0){
				echo 'Anterior';
			}else{
				?>
				<a href="got.php?c=<?php echo $c-1;?>">Anterior</a>
				<?php
			}
			?>

			<?php 
			if($c==0){
				echo 'Anterior';
			}else{
				echo '<a href="got.php?c='.($c-1).'">Anterior</a>';
			}
			?>

			 - <?php echo $capitulos[$c]; ?> - 
			
			<?php
			if($c==9){
				echo 'Siguiente';
			}else{
				?>
				<a href="got.php?c=<?php echo $c+1;?>">Siguiente</a>
				<?php
			}
			?>
			
			<hr>	
		</header>
		<section><?php echo $contenidos[$c]; ?></section>
	</section>
	
</body>
</html>