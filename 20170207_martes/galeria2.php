<?php  
//Establezco variables para mi MINIPROGRAMA
$ninicio=1; //El numero de mi primera imagen
$nfinal=4; //El numero de mi ultima imagen

//Recojo la variable numero
if(isset($_GET['numero'])){
	$numero=$_GET['numero'];
}else{
	$numero=1;
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>galeria2.php</title>
	<style>
		span{
			font-size:30px;
		}
	</style>
</head>
<body>
	<h1>GALERIA 2 . PHP</h1>
	<?php 
		if($numero==1){
			echo 'Anterior';
		}else{
			?>
			<a href="galeria2.php?numero=<?php echo ($numero-1); ?>">Anterior</a>
			<?php
		}
	?>

	<span><img src="imagenes/<?php echo $numero; ?>.jpg" width="200"></span>
	
	<?php 
		if($numero==4){
			echo 'Siguiente';
		}else{
			?>
			<a href="galeria2.php?numero=<?php echo ($numero+1); ?>">Siguiente</a>
			<?php
		}
	?>

</body>
</html>