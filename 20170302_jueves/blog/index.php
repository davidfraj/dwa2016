<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Blog</title>
</head>
<body>
	<?php  
	
	//1.- Conectar a la base de datos
	$servidor='localhost';
	$usuario='root';
	$clave='';
	$base='blog';
	$conexion = mysqli_connect($servidor, $usuario, $clave, $base);
	mysqli_set_charset($conexion, 'utf8');

	//2.- Pensar QUÈ Preguntar a la base de datos
	//Standard query language (SQL) //SELECT UPDATE DELETE INSERT
	$sql="SELECT id,titulo,contenido,autor,imagen,fecha FROM posts";

	//3.- Preguntar a la base de datos
	$consulta=mysqli_query($conexion, $sql);

	//4.- Procesar la respuesta
	while($r=mysqli_fetch_array($consulta)){
		?>
		<article style="border:1px solid black; padding:20px; margin: 20px;">
			<header><h2><?php echo $r['titulo']; ?></h2></header>
			<section><?php echo $r['contenido']; ?></section>
			<footer>
				<?php echo $r['autor']; ?> - <?php echo $r['fecha']; ?>
			</footer>
		</article>
		<hr>
		<?php
	}

	//5.- Desconectar de la base de datos
	mysqli_close($conexion);
	?>
</body>
</html>