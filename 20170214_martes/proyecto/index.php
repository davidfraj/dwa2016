<?php 
//Voy a recoger el $_GET['p']
if(isset($_GET['p'])){
	$p=$_GET['p'];
}else{
	$p='inicio.php';
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Proyecto completo en php</title>
	<style>
		ul li{
			display: inline-block;
			padding: 0px;
		}
	</style>
	<link rel="stylesheet" href="css/estilos.css">
</head>
<body>
	<section id="principal">
		<nav>
			<?php include('includes/menu.php'); ?>
		</nav>
		<header>
			<?php include('includes/encabezado.php'); ?>	
		</header>
		<section>
			<hr>
			<?php include('paginas/'.$p); ?>
			<hr>	
		</section>
		<footer>
			<?php include('includes/menu.php'); ?>
			<?php include('includes/pie.php'); ?>
		</footer>
	</section>
</body>
</html>