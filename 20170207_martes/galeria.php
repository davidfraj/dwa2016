<?php  
//Recojo la variable numero
if(isset($_GET['numero'])){
	$numero=$_GET['numero'];
}else{
	$numero=1;
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>galeria.php</title>
	<style>
		span{
			font-size:30px;
		}
	</style>
</head>
<body>
	<a href="galeria.php?numero=<?php echo ($numero-1); ?>">Anterior</a>
	<span><img src="imagenes/<?php echo $numero; ?>.jpg" width="200"></span>
	<a href="galeria.php?numero=<?php echo ($numero+1); ?>">Siguiente</a>
</body>
</html>