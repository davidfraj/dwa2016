<?php  
//Establezco variables para mi MINIPROGRAMA
$ninicio=1; //El numero de mi primera imagen
$nfinal=7; //El numero de mi ultima imagen

//Me creo un vector, con los nombres de imagenes
$nombres=array('', 'Flor naranja', 'Desierto', 'Flor azul', 'Medusa', 'Faro', 'Pinguinos', 'Tulipanes');

//Recojo la variable numero
if(isset($_GET['numero'])){
	$numero=$_GET['numero'];
}else{
	$numero=1;
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>galeria3.php</title>
</head>
<body>
	<h1>GALERIA 3 . PHP - <?php echo $nombres[$numero];?></h1>
	<?php 
		if($numero==$ninicio){
			echo '<img src="imagenes/ninguna.jpg" width="50">';
		}else{
			?>
			<a href="galeria3.php?numero=<?php echo ($numero-1); ?>"><img src="imagenes/<?php echo ($numero-1); ?>.jpg" width="50"></a>
			<?php
		}
	?>

	<span><img src="imagenes/<?php echo $numero; ?>.jpg" width="200"></span>
	
	<?php 
		if($numero==$nfinal){
			?>
			<img src="imagenes/ninguna.jpg" width="50">
			<?php
		}else{
			?>
			<a href="galeria3.php?numero=<?php echo ($numero+1); ?>"><img src="imagenes/<?php echo ($numero+1); ?>.jpg" width="50"></a>
			<?php
		}
	?>

</body>
</html>