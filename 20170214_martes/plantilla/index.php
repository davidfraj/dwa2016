<?php 
//Voy a recoger el $_GET['p']
if(isset($_GET['p'])){
	$p=$_GET['p'];
}else{
	$p='blog.php';
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Beach Sunset by Free CSS Templates</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<div id="header">
		<div id="menu">
			<ul>
				<li class="current_page_item"><a href="#">Home</a></li>
				<li><a href="index.php?p=blog.php">Blog</a></li>
				<li><a href="index.php?p=photos.php">Photos</a></li>
				<li><a href="index.php?p=about.php">About</a></li>
				<li><a href="index.php?p=links.php">Links</a></li>
				<li><a href="index.php?p=contact.php">Contact</a></li>
			</ul>
		</div>
		<div id="search">
			<form method="get" action="#">
				<fieldset>
				<input type="text" name="s" id="search-text" size="15" />
				<input type="submit" id="search-submit" value="Search" />
				</fieldset>
			</form>
		</div>
	</div>
	<div id="logo">
		<h1><a href="#">Beach Sunset </a></h1>
		<p><em> template design by <a>Free CSS Templates</a></em></p>
	</div><hr />
	<div id="page"><div class="inner_copy"></div>
		<div id="content">
			

			<?php include('paginas/'.$p); ?>



		</div>
		<div id="sidebar">
			<ul>
				<li>
					<h2>Aliquam tempus</h2>
					<p>Mauris vitae nisl nec metus placerat perdiet est. Phasellus dapibus semper urna. Pellentesque ornare, orci in consectetuer hendrerit, volutpat.</p>
				</li>
				<li id="calendar">
					<h2>Calendar</h2>
					<div id="calendar_wrap">
						<table summary="Calendar">
							<caption>March 2008</caption>
							<thead>
								<tr><th abbr="Monday" scope="col" title="Monday">M</th><th abbr="Tuesday" scope="col" title="Tuesday">T</th><th abbr="Wednesday" scope="col" title="Wednesday">W</th><th abbr="Thursday" scope="col" title="Thursday">T</th><th abbr="Friday" scope="col" title="Friday">F</th><th abbr="Saturday" scope="col" title="Saturday">S</th><th abbr="Sunday" scope="col" title="Sunday">S</th></tr>
							</thead>
							<tfoot>
								<tr><td abbr="February" colspan="3" id="prev"><a href="#" title="">&laquo; Feb</a></td><td class="pad">&nbsp;</td><td abbr="April" colspan="3" id="next"><a href="#" title="">Apr &raquo;</a></td></tr>
							</tfoot>
							<tbody>
								<tr><td colspan="5" class="pad">&nbsp;</td><td>1</td><td>2</td></tr>
								<tr><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td></tr>
								<tr><td>10</td><td id="today">11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td></tr>
								<tr><td>17</td><td>18</td><td>19</td><td>20</td><td>21</td><td>22</td><td>23</td></tr>
								<tr><td>24</td><td>25</td><td>26</td><td>27</td><td>28</td><td>29</td><td>30</td></tr>
								<tr><td>31</td><td class="pad" colspan="6">&nbsp;</td></tr>
							</tbody>
						</table>
					</div>
				</li>
				<li>
					<h2>Turpis nulla</h2>
					<ul>
						<li><a href="#"></a><a href="#">Nec metus sed donec</a></li>
						<li><a href="#">Magna lacus bibendum mauris</a></li>
						<li><a href="#">Velit semper nisi molestie</a></li>
						<li><a href="#">Eget tempor eget nonummy</a></li>
						<li><a href="#">Nec metus sed donec</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div style="clear:both">&nbsp;</div>
	</div>
	<div id="footer"><div class="fleft"><p>Copyright statement.</p></div><div class="fright"><p>Busque m&aacute;s plantillas web gratis <a href="http://www.mejoresplantillasgratis.es" target="_blank">en MPG.es</a>.</p></div><div class="fcenter"><p>Design by: Design by <a href="http://www.freecsstemplates.org/">Free CSS Templates</a></p></div><div class="fclear"></div></div>
</div>
</body>
</html>
