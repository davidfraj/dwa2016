<?php  
//datos de mi producto
$nombre='Teclado bluetooth v2.0';
$precio=112.20;
$unidades=3;
$cantidadIva=24;
$imagen='teclado.jpg';
$ancho=200;

//Realizo calculos
$total=$precio*$unidades;
$iva=$total*$cantidadIva/100;
$importe=$total+$iva;

//Atajos en los calculos
//Sumas
$numero=1;

$numero=$numero+1; //numero vale 2
$numero++; //numero vale 3
$numero+=2; //numero vale 5

$texto='hola que tal';
$texto=$texto.', yo bien';

$texto='hola que tal';
$texto.=', yo bien';

$resultado='';
$precio=120;
$resultado.='El precio es '.$precio.' Euros.';

$texto='hola que tal $precio';
$texto="hola que tal $precio";

$texto='<p class="tipo1">Esto es un precio: '.$precio.'</p>';

//Vectores en PHP (normales)

$v=array(7,18,21);

echo $v[1]; //Muestra 18

$v2[0]=7;
$v2[1]=18;
$v2[2]=21;

$v3[]=7;
$v3[]=18;
$v3[]=21;

$v3[count($v3)]=7;

$v4['uno']=1;
$v4['dos']='El numero correspondiente';
$v4['tres']=3;

$v4=array('uno'=>1, 'dos'=>'El numero correspondiente', 'tres'=>3);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>repaso.php</title>
</head>
<body>
	<table>
		<tr>
			<td>Nombre</td>
			<td><?php echo $nombre; ?></td>
		</tr>
		<tr>
			<td>Precio:</td>
			<td><?php echo $precio; ?></td>
		</tr>
		<tr>
			<td>Unidades:</td>
			<td><?php echo $unidades; ?></td>
		</tr>
		<tr>
			<td>Total:</td>
			<td><?php echo $total; ?></td>
		</tr>
		<tr>
			<td>Iva (<?php echo $cantidadIva; ?> %):</td>
			<td><?php echo $iva; ?></td>
		</tr>
		<tr>
			<td>Importe:</td>
			<td><?php echo $importe; ?></td>
		</tr>
		<tr>
			<td>Imagen:</td>
			<td><img src="<?php echo $imagen; ?>" width="<?php echo $ancho; ?>" style="width:<?php echo $ancho;?>;px;"></td>
		</tr>
	</table>

	<hr>

	<p><?php echo $texto; ?></p>

	<?php echo $v3[2]; ?>

</body>
</html>
